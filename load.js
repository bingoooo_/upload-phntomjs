'use strict';

var page = require('webpage').create(); // used to pick up a webpage from internet
var system = require('system');			// ability to use system arguments

// page settings
var Requests = 0;
var Response = 0;
page.onConsoleMessage = function(msg){	// activate console message
	console.log('Received from console :', msg);
};
page.onResourceRequested = function(request){
	Requests += 1;
	console.log('Request:', JSON.stringify(request, undefined, 4));
	console.log('Requests:', Requests);
	console.log('Responses:', Response);
};
page.onResourceReceived = function(response){
	Response += 1;
	console.log('Received:', JSON.stringify(response, undefined, 4));
	console.log('Requests:', Requests);
	console.log('Responses:', Response);
};
// page.viewPortSize = {width: 1024, heigth: 768};
// page.clipRect = {top: 0, left:0, width: 1024, heigth: 768};


var loading_time, rendering_time;		// loading and rendering time variables
var adress;								// url used in system argument


/*
*	use of system arguments
*/
if(system.args.length === 1){
	console.log('Usage: loadspeed.js <some URL>');
	phantom.exit();
}

/*
*	Main program
*/

// Start
loading_time = Date.now();
adress = system.args[1];
page.open(adress, function(status){

	// First pass getting the title of the page
	console.log('Status: ', status);
	var title = page.evaluate(function(){
		console.log('This line is not meant to be displayed... however...');
		return document.title;
	});
	console.log('Processing :', title);
	if(status == 'success'){
		// Second pass getting loading time
		loading_time = Date.now() - loading_time;
		console.log('Page Loaded in',loading_time ,'ms, rendering...');
		// Third pass getting rendering time
		rendering_time = Date.now();
		page.render(title+'.png');
		rendering_time = Date.now() - rendering_time;
		console.log(rendering_time, 'ms.');
	}
	phantom.exit();
});
