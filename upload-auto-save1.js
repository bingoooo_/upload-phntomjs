'use strict';

var fs = require('fs');
var page = require('webpage').create();
page.viewPortSize = {width: 1920, heigth: 1024};
page.clipRect = {top: 0, left:0, width: 1920, heigth: 1024};
console.log('The default user agent is', page.settings.userAgent);

page.onConsoleMessage = function(msg){	// activate console message
	console.log(msg);
};
page.onLoadFinished = function(data){
	if(data === 'success'){
		console.log('Phase', phase,'Loaded');
		Current();
		if(phase == 0){
			Progress(phase);
			Login(datas);
		} else if(phase == 1){
			Progress(phase);
			Select(datas);
		} else if(phase == 2){
			Progress(phase);
			setTimeout(function(){
				FolderSearch(datas);
			}, 1000);
		} else if(phase == 3){
			Progress(phase);
			setTimeout(function(){
				AddContent(datas);
			}, 1000);
		}
		phase += 1;
	} else {
		Progress(phase);
	}
}

// Get external datas
var datas = fs.read('./file.json');
var datas = JSON.parse(datas);
var list = fs.list(datas.filesFolder);
datas.list = [];
for (var i = 0; i < list.length; i++){
	if((list[i] !== '.') && (list[i] !== '..')) datas.list.push(list[i]);
}
console.log(typeof(datas), datas.targetFolder);
console.log(typeof(datas.list), datas.list);

// Phase variable
var gap = 6000;
var phase = 0;

var Start = function(){
	page.open(datas.baseURL, function(status){
		if(status !== 'success'){
			Exit(0);
		}
	});
}

var Login = function(datas){
	console.log('Login Datas', datas);
	page.evaluate(function(datas){
		console.log('Login', datas);
		var login = document.getElementById('login_userid');
		var pwd = document.getElementById('login_pwd');
		var submit = document.getElementsByClassName("btn");
		login.value = datas.user;
		pwd.value = datas.pwd;
		console.log('btn:', submit.length);
		var click = setTimeout(function() {
			submit[1].click();
			console.log('Ended Login');
		}, 100);
	}, datas);
}

var Select = function(datas){
	console.log('Select Datas', datas);
	page.evaluate(function(datas){
		var links = document.getElementsByTagName('a');
		console.log('links:', links.length);
		for (var i = 0; i < links.length; i++){
			if(links[i].getAttribute('href') === datas.Cours){
				console.log(i, links[i].innerHTML);
				links[i].click();
			}
		}
	}, datas, page);
}

var FolderSearch = function(datas){
	console.log('Folder Search Datas', datas);
	var getOut = false;
	var create = false;
	getOut = page.evaluate(function(datas, Search){
		var path = datas.targetFolder;
		var path = path.split('\\');
		var folders = [];
		var finalFolder = 0;
		console.log('Path', path);
		var els = document.getElementsByClassName('label-for-radio');
		console.log('labels:', typeof(els), els.length);
		finalFolder = Search(path, els);
		if(finalFolder === 0){
			console.log('Folder not found');
			return true;
		}
		console.log('Click');
		els[finalFolder].click();
		return false;
	}, datas, Search);
	if (getOut) Exit(0);
	setTimeout(function(){
		getOut = page.evaluate(function(datas){
			var table = document.querySelector('.items');
			console.log('table:', table);
			var bottomTable = table.getElementsByTagName('tr');
			console.log('items:', bottomTable.length);
			for (var i = 0; i < bottomTable.length; i++){
				var child = bottomTable[i].lastChild;
				console.log('child:', i, child);
			}
			var selected = bottomTable[1];
			if(bottomTable[1].lastChild.querySelector('a') !== null){
				bottomTable[1].lastChild.querySelector('a').click();
				setTimeout(function(){
					var resources = document.querySelector('.training-materials');
					console.log('Resources:', resources.innerHTML);
					resources.click();
					return false;
				}, 100);
			} else {
				console.log(bottomTable[1].lastChild.firstChild.innerHTML, 'creating new course');
				var newCourse = document.querySelector('.new-course');
				console.log(newCourse.innerHTML);
				newCourse.click();
				setTimeout(function(){
					var title = document.getElementById('LearningCourse_name');
					var type = document.getElementById('LearningCourse_course_type_' + datas.type);
					var photos = document.getElementsByClassName('sub-item');
					var frame = document.getElementById('mceu_33').firstChild;
					var description = frame.contentWindow.document.getElementById('tinymce');
					var code = document.getElementById('LearningCourse_code');
					var inputs = document.getElementsByTagName('a');
					title.value = datas.courseName;
					type.click();
					photos[datas.photo].click();
					description.value = datas.description;
					code.value = datas.code;
					setTimeout(function(){
						for (var i = 0; i < inputs.length; i++){
							if(inputs[i].getAttribute('data-callback') === "submit-testCallback"){
								console.log(inputs[i].innerHTML);
								inputs[i].click();
								break;
							}
						}
						setTimeout(function(){
							console.log('Course', title.value, 'created');
						}, 500);
					}, 500);
				}, 2000);
				return false;
			}
		}, datas);
	if (getOut) Exit(0);
	}, 1500);
	// setTimeout(function(){
	// 	page.render('coco.png');
	// 	phantom.exit();
	// }, 10000);
	// Exit(10000);
}

var AddContent = function(datas){
	console.log('AddContent Datas', datas);
	page.render('test.png');
}

var Search = function(path, table){
	var out = 0;
	for (var i = 0; i < path.length; i++){
		console.log('Searching for', path[i]);
		for (var j = out; j < table.length; j++){
			if(table[j].innerHTML === path[i]){
				out = j;
				console.log('Out', out);
				break;
			}
		}
	}
	return out;
}

var Progress = function(phase){
	console.log('Phase', phase);
	console.log('Progress', page.loadingProgress);
	console.log('Datas', datas);
}

var Current = function(){
	page.evaluate(function(){
		var node = document.querySelector('.last-node');
		if (node !== null) console.log('>>>>>>>>>>>> Current Position', node.innerHTML);
	});
}

var Exit = function(time){
	setTimeout(function(){
		console.log('Exit', time/1000);
		phantom.exit();
	}, time);
}

Start();