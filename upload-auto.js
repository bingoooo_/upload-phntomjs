/*
* This Code is Spécific to yesnyoulearning.com, use it else where would'nt work,
* but you can be inspired by this work and make it suit the websites you whish to
* automate.
*/


'use strict';

var fs = require('fs');
var page = require('webpage').create();
var system = require('system');
page.viewPortSize = {width: 1920, heigth: 1024};
page.clipRect = {top: 0, left:0, width: 1920, heigth: 1024};
console.log('The default user agent is', page.settings.userAgent);

page.onConsoleMessage = function(msg){	// activate console message
	console.log(msg);
};
var currentFile = 0;
page.onFilePicker = function(msg){
	var file = datas.path + datas.filesFolder + datas.list[currentFile];
	console.log('Triying to upload file:', file);
	if (currentFile < datas.list.length) {
		currentFile += 1;
	} else {
		Exit(0);
	}
	return file;
};
page.onLoadFinished = function(data){
	if(data === 'success'){
		console.log('Phase', phase,'Loaded');
		Current();
		if(phase == 0){
			Progress(phase, 'Login');
			Login(datas);
		} else if(phase == 1){
			Progress(phase, 'Select');
			Select(datas);
		} else if(phase == 2){
			Progress(phase, 'Searching Folder');
			setTimeout(function(){
				FolderSearch(datas);
			}, 1000);
		} else if(phase == 3){
			Progress(phase, 'Add content');
			setTimeout(function(){
				AddContent(datas);
			}, 1000);
		} else if(phase == 4){
			Progress(phase);
			Exit(0);
		}
		phase += 1;
	} else {
		Progress(phase);
	}
}

// Get external datas
var datas = fs.read('./file.json');
var datas = JSON.parse(datas);
var list = fs.list(datas.filesFolder);
datas.list = [];
for (var i = 0; i < list.length; i++){
	if((list[i] !== '.') && (list[i] !== '..')) datas.list.push(list[i]);
}
datas.path = fs.workingDirectory + '/';
console.log(typeof(datas), datas.targetFolder);
console.log(typeof(datas.list), datas.list);

// Phase variable
var gap = 6000;
var phase = 0;
var start_time, end_time;

// Starting Point, executed
var Start = function(){
	start_time = Date.now();
	var os = system.os.name;
	console.log('Checking OS type:', os);
	if (os === 'windows') console.log('What a shame');
	if (os === 'linux') console.log('Good choice :)');
	page.open(datas.baseURL, function(status){
		if(status !== 'success'){
			Exit(0);
		}
	});
}

var Login = function(datas){
	console.log('Login Datas', datas);
	page.evaluate(function(datas){
		console.log('Login', datas);
		var login = document.getElementById('login_userid');
		var pwd = document.getElementById('login_pwd');
		var submit = document.getElementsByClassName("btn");
		login.value = datas.user;
		pwd.value = datas.pwd;
		console.log('btn:', submit.length);
		var click = setTimeout(function() {
			submit[1].click();
			console.log('Ended Login');
		}, 100);
	}, datas);
}

// Select the section in which you want to modify things, obviously the courses
var Select = function(datas){
	console.log('Select Datas', datas);
	page.evaluate(function(datas){
		var links = document.getElementsByTagName('a');
		console.log('links:', links.length);
		for (var i = 0; i < links.length; i++){
			if(links[i].getAttribute('href') === datas.section){
				console.log(i, links[i].innerHTML);
				links[i].click();
			}
		}
	}, datas, page);
}

var FolderSearch = function(datas){
	console.log('Folder Search Datas', datas);
	var openFolder = page.evaluate(function(datas){
		var Search = function(path, table, index){
			console.log('Searching for', path[index]);
			for (var i = 0; i < table.length; i++){
				if (table[i].innerHTML === path[index]){
					var pNode = table[i].parentNode.parentNode.parentNode.parentNode.parentNode.querySelector('ul');
					var list = pNode.getElementsByClassName('label-for-radio');
					console.log('New List Length', list.length);
					if (index < path.length - 2) {
						index++;
						Search(path, list, index);
					} else {
						console.log(list[0].innerHTML);
						finalFolder = list[0];
					}
					break;
				}
			}
			
		}
		var path = datas.targetFolder;
		var path = path.split('\\');
		var folders = [];
		var finalFolder = null;
		console.log('Path', path);
		var els = document.getElementsByClassName('label-for-radio');
		console.log('labels:', typeof(els), els.length);
		Search(path, els, 0);
		console.log(finalFolder);
		if(finalFolder === 0){
			console.log('Folder not found');
			phantom.exit();
		}
		console.log('Click');
		// els[finalFolder].click();
		finalFolder.click();
	}, datas);
	setTimeout(function(){
		var clickCourse = page.evaluate(function(datas, ClickCourse){
			console.log('Loading course', datas.courseName);
			var course = ClickCourse(datas);
			console.log(course.innerHTML);
			course.querySelector('a').click();
		}, datas, ClickCourse);
		var training = setInterval(function(){
			var loading = document.querySelector('grid-view-loading');
			if(!loading) {
				var trainingMaterials = page.evaluate(function(datas){
					console.log('Going to', datas.courseName, 'training materials');
					var resources = document.querySelector('.training-materials');
					if(resources){
						console.log('Resources:', resources.innerHTML);
						resources.click();
					} else console.log('No Courses');
				}, datas);
				clearInterval(training);
			}
		}, 500);
	}, 1000);
}

var AddContent = function(datas){
	console.log('AddContent Datas', datas);
	var add = page.evaluate(function(datas){
		var Add = function(datas, index){
			var addModule = document.querySelector('.is-zip');
			console.log('Click SCORM', addModule);
			addModule.click();
			setTimeout(function(){
				var addbutton = document.getElementById('pickfiles').parentNode;
				console.log('Open Pick Files', addbutton);
				addbutton.querySelector('a').click();
			}, 2000);
			setTimeout(function(){
				var checkstatusbar = setInterval(function(){
					var bar = document.getElementById('uploader-progress-bar');
					console.log('Bar styles', bar.getAttribute('style'));
					if(bar.getAttribute('style') == 'width: 100%;'){
						clearInterval(checkstatusbar);
						setTimeout(function(){
							var progress = document.querySelector('.upload-progress').getAttribute('style');
							console.log('Progress ?', progress);
							var bar = document.getElementById('uploader-progress-bar');
							console.log('Bar classes', bar.getAttribute('class'));
							console.log('Bar styles', bar.getAttribute('style'));
							console.log('Click Validation Button', document.getElementById('player-save-uploaded-lo').value);
							document.getElementById('player-save-uploaded-lo').click();
							setTimeout(function(){
								console.log('Upload Status');
								var upload = setInterval(function(){
									var uploading = document.getElementById('upload-stuff-message');
									var style = uploading.getAttribute('style');
									if (style == 'display: none;') {
										console.log('Finished Upload');
										clearInterval(upload);
										if(index < datas.list.length -1){
											index += 1;
											Add(datas, index);
										}
									}
								}, 1000);
							}, 1000);
						}, 1000);
					}
				}, 500);
			}, 2001);
		}
		Add(datas, 0);
	}, datas);
}


var ClickCourse = function(datas) {
	var oddcourses = document.getElementsByClassName('odd');
	var evencourses = document.getElementsByClassName('even');
	console.log('Courses Present', oddcourses.length + evencourses.length);
	for (var i = 0; i < oddcourses.length; i++){
		if (oddcourses[i].children[3].innerHTML === datas.courseName){
			console.log('Course', datas.courseName, 'found.');
			return oddcourses[i].lastChild;
		};
	}
	for (var i = 0; i < evencourses.length; i++) {
		if (evencourses[i].children[3].innerHTML === datas.courseName){
			console.log('Course', datas.courseName, 'found.');
			return evencourses[i].lastChild;
		};
	};
}

var Progress = function(phase, title){
	console.log('Phase', phase, title);
	console.log('Progress', page.loadingProgress);
	console.log('Datas', datas);
}

var Current = function(){
	page.evaluate(function(){
		var node = document.querySelector('.last-node');
		if (node !== null) console.log('>>>>>>>>>>>> Current Position', node.innerHTML);
	});
}

var Exit = function(time){
	setTimeout(function(){
		console.log('Exit', time/1000);
		end_time = Date.now();
		var total_time = Math.floor((end_time - start_time)/60000);
		console.log('time', total_time)
		phantom.exit();
	}, time);
}

Start();