# Upload automatique sur yesnyoulearning  

Il s'agit d'un script utilisant phantomjs ne traitant particulièrement que les pages du site yesnyoulearning  
  
Seuls les personnes possédant un compte admin peuvent entrer dans les options de modification des cours.  
  
N'oubliez pas de modifier le fichier file.json.example avec vos identifiants et de modifier le nom du fichier lui même en file.json
  
Pour exécuter les script :  
	`phantomjs upload-auto.js`  
	`phantomjs delete-files-auto.js` 
  
  
Actuellement le code est relativement vilain, mais il fait sont travail... 