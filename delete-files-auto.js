'use strict';

var fs = require('fs');
var page = require('webpage').create();
var system = require('system');
page.viewPortSize = {width: 1920, heigth: 1024};
page.clipRect = {top: 0, left:0, width: 1920, heigth: 1024};
console.log('The default user agent is', page.settings.userAgent);

page.onConsoleMessage = function(msg){	// activate console message
	console.log(msg);
};
page.onFilePicker = function(msg){
	var file = datas.path + datas.filesFolder + datas.list[1];
	console.log('Triying to upload file:', file);
	return file;
};
page.onLoadFinished = function(data){
	if(data === 'success'){
		console.log('Phase', phase,'Loaded');
		Current();
		if(phase == 0){
			Progress(phase, 'Login');
			Login(datas);
		} else if(phase == 1){
			Progress(phase, 'Select');
			Select(datas);
		} else if(phase == 2){
			Progress(phase, 'Searching Folder');
			setTimeout(function(){
				FolderSearch(datas, currentFolder, currentCourse);
			}, 1000);
		} else if(phase == 3){
			Progress(phase, 'Delete Content');
			setTimeout(function(){
				DelContent(datas);
			}, 1000);
		} else if(phase == 4){
			Progress(phase);
			Exit(0);
		}
		phase += 1;
	} else {
		Progress(phase);
	}
}

// Get external datas
var datas = fs.read('./delete.json');
var datas = JSON.parse(datas);
var list = fs.list(datas.filesFolder);
datas.list = [];
for (var i = 0; i < list.length; i++){
	if((list[i] !== '.') && (list[i] !== '..')) datas.list.push(list[i]);
}
datas.path = fs.workingDirectory + '/';
console.log(typeof(datas), datas.targetFolder);
console.log(typeof(datas.list), datas.list);

// Phase variable
var gap = 6000;
var phase = 0;
var currentFolder = 0;
var currentCourse = 0;
var start_time, end_time;

// Starting Point, executed
var Start = function(){
	start_time = Date.now();
	var os = system.os.name;
	console.log('Checking OS type:', os);
	if (os === 'windows') console.log('What a shame');
	if (os === 'linux') console.log('Good choice :)');
	page.open(datas.baseURL, function(status){
		if(status !== 'success'){
			Exit(0);
		}
	});
}

var Login = function(datas){
	console.log('Login Datas', datas);
	page.evaluate(function(datas){
		console.log('Login', datas);
		var login = document.getElementById('login_userid');
		var pwd = document.getElementById('login_pwd');
		var submit = document.getElementsByClassName("btn");
		login.value = datas.user;
		pwd.value = datas.pwd;
		console.log('btn:', submit.length);
		var click = setTimeout(function() {
			submit[1].click();
			console.log('Ended Login');
		}, 100);
	}, datas);
}

var Select = function(datas){
	console.log('Select Datas', datas);
	page.evaluate(function(datas){
		var links = document.getElementsByTagName('a');
		console.log('links:', links.length);
		for (var i = 0; i < links.length; i++){
			if(links[i].getAttribute('href') === datas.section){
				console.log(i, links[i].innerHTML);
				links[i].click();
			}
		}
	}, datas, page);
}

var FolderSearch = function(datas, currentFolder, currentCourse){
	console.log('Folder Search Datas', datas);
	var file = datas.actions.delete.files;
	console.log('Files', file[0].folder)
	var openFolder = page.evaluate(function(file, currentFolder, currentCourse, Search){
		var path = file[currentFolder].folder;
		var path = path.split('\\');
		var folders = [];
		var finalFolder = 0;
		console.log('Path', path);
		var els = document.getElementsByClassName('label-for-radio');
		console.log('labels:', typeof(els), els.length);
		finalFolder = Search(path, els);
		if(finalFolder === 0){
			console.log('Folder not found');
			return true;
		}
		console.log('Click');
		els[finalFolder].click();
		return false;
	}, file, currentFolder, currentCourse, Search);
	setTimeout(function(){
		var clickCourse = page.evaluate(function(file, currentFolder, currentCourse, ClickCourse){
			console.log('Loading course', file[currentFolder].courses[currentCourse]);
			var checkCourse = setInterval(function(){
				var course = ClickCourse(file[currentFolder].courses[currentCourse]);
				if(course.innerHTML){
					console.log(course.innerHTML);
					course.querySelector('a').click();
					clearInterval(checkCourse);
				}
			}, 1000);
		}, file, currentFolder, currentCourse, ClickCourse);
		var training = setInterval(function(){
			var loading = document.querySelector('grid-view-loading');
			if(!loading) {
				var trainingMaterials = page.evaluate(function(file, currentFolder, currentCourse){
					console.log('Going to', file[currentFolder].courses[currentCourse], 'training materials');
					var resources = document.querySelector('.training-materials');
					console.log('Resources:', resources.innerHTML);
					resources.click();
				}, file, currentFolder, currentCourse);
				clearInterval(training);
			}
		}, 1010);
	}, 1000);
}

var DelContent = function(datas){
	var clickSuppress = page.evaluate(function(datas){
		var position = document.querySelector('.breadcrumb').lastChild;
		console.log('Delete Files in', position.innerHTML);
		console.log('Deleting');
		var toDel = document.querySelector('a[class="p-hover menuitem-delete-lo"]');
		if (toDel) {
			console.log('Datas', toDel);
			toDel.click();
			return true;
		} else return false;
	}, datas);
	var del = page.evaluate(function(datas){
		var self = this;
		var checkmodal = setInterval(function(){
			var modal = document.getElementsByClassName('modal-footer')[1];
			if(modal.innerHTML){
				clearInterval(checkmodal)
				console.log(modal.innerHTML);
				modal.firstChild.click();
				setTimeout(function(){
					var next = document.querySelector('a[class="p-hover menuitem-delete-lo"]');
					console.log('Next ?', next);
					if(next) console.log('Delete next content');
				}, 2000);
			}
		}, 1000);
	}, datas);
	if (!clickSuppress) page.reload();
	var reload = setTimeout(function(){
		phase = 3;
		console.log(phase);
		page.reload();
	}, 3000);
}


var Search = function(path, table){
	var out = 0;
	for (var i = 0; i < path.length; i++){
		console.log('Searching for', path[i]);
		for (var j = out; j < table.length; j++){
			if(table[j].innerHTML === path[i]){
				out = j;
				console.log('Out', out);
				break;
			}
		}
	}
	return out;
}

var ClickCourse = function(name) {
	var oddcourses = document.getElementsByClassName('odd');
	var evencourses = document.getElementsByClassName('even');
	console.log('Courses Present', oddcourses.length + evencourses.length);
	for (var i = 0; i < oddcourses.length; i++){
		if (oddcourses[i].children[3].innerHTML === name){
			console.log('Course', name, 'found.');
			return oddcourses[i].lastChild;
		};
	}
	for (var i = 0; i < evencourses.length; i++) {
		if (evencourses[i].children[3].innerHTML === name){
			console.log('Course', name, 'found.');
			return evencourses[i].lastChild;
		};
	};
}

var Progress = function(phase){
	console.log('Phase', phase);
	console.log('Progress', page.loadingProgress);
	console.log('Datas', datas);
}

var Current = function(){
	page.evaluate(function(){
		var node = document.querySelector('.last-node');
		if (node !== null) console.log('>>>>>>>>>>>> Current Position', node.innerHTML);
	});
}

var Exit = function(time){
	setTimeout(function(){
		end_time = Date.now();
		console.log('Time :', Math.floor((end_time - start_time)/1000));
		console.log('Exit', time/1000);
		phantom.exit();
	}, time);
}

Start();